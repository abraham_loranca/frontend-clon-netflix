import 'package:flutter/material.dart';

class ItemImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> _imagenes = [
      "https://i.pinimg.com/originals/fd/da/49/fdda49c514c4be0367c1b7bc8181c48b.jpg",
      "https://mir-s3-cdn-cf.behance.net/project_modules/1400/1f9d0a98111957.5ed50e8a18fb4.png",
      "https://i.pinimg.com/736x/b1/b7/8f/b1b78f5b50e463b68519a1ccae02e0ae.jpg",
      "https://i1.wp.com/hiramnoriega.com/wp-content/uploads/2021/01/324432423434432.jpg?resize=500%2C741&ssl=1",
      "https://i.pinimg.com/originals/93/6a/ac/936aac6f166d48d71512bf94b53803f4.png"
    ];
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _imagenes.length,
        itemBuilder: (context, index) {
          return Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0),
            child: Image.network(
              _imagenes[index],
              width: 100.0,
              fit: BoxFit.cover,
            ),
          );
        });
  }
}
