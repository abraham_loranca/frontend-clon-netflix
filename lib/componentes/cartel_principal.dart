import 'package:clone_netflix/componentes/nav_bar_superior.dart';
import 'package:flutter/material.dart';

class CartelPrincipal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[this.cabecera(), this.infoserie(), this.botonera()],
    );
  }

  Widget cabecera() {
    return Stack(children: <Widget>[
      Image.network(
        'https://los40es00.epimg.net/los40/imagenes/2020/06/29/bigbang/1593433117_130907_1593433381_noticia_normal.jpg',
        height: 350.0,
        fit: BoxFit.cover,
        width: double.maxFinite,
      ),
      Container(
        height: 350.0,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.center,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.black38, Colors.black])),
      ),
      SafeArea(child: NavBarSuperior())
    ]);
  }

  Widget infoserie() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('Telenovelesco',
            style: TextStyle(color: Colors.white, fontSize: 10.0)),
        SizedBox(
          width: 6.0,
        ),
        Icon(
          Icons.fiber_manual_record,
          color: Colors.white,
          size: 5.0,
        ),
        SizedBox(
          width: 6.0,
        ),
        Text('Suspenso insostenible',
            style: TextStyle(color: Colors.white, fontSize: 10.0)),
        SizedBox(
          width: 6.0,
        ),
        Icon(
          Icons.fiber_manual_record,
          color: Colors.white,
          size: 5.0,
        ),
        SizedBox(
          width: 6.0,
        ),
        Text('De suspenso',
            style: TextStyle(color: Colors.white, fontSize: 10.0)),
        SizedBox(
          width: 6.0,
        ),
        Icon(
          Icons.fiber_manual_record,
          color: Colors.white,
          size: 5.0,
        ),
        SizedBox(
          width: 6.0,
        ),
        Text('Adolescentes',
            style: TextStyle(color: Colors.white, fontSize: 10.0)),
      ],
    );
  }

  Widget botonera() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Icon(Icons.check, color: Colors.white),
              SizedBox(height: 3.0),
              Text(
                'Mi lista',
                style: TextStyle(color: Colors.white, fontSize: 10.0),
              )
            ],
          ),
          FlatButton.icon(
              onPressed: () {},
              color: Colors.white,
              icon: Icon(
                Icons.play_arrow,
                color: Colors.black,
              ),
              label: Text('Reproducir')),
          Column(
            children: <Widget>[
              Icon(Icons.info_outline, color: Colors.white),
              SizedBox(height: 3.0),
              Text(
                'Info',
                style: TextStyle(color: Colors.white, fontSize: 10.0),
              )
            ],
          ),
        ],
      ),
    );
  }
}
