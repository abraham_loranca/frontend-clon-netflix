import 'package:flutter/material.dart';

class ItemRedondo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> _imagenes = [
      "https://seriescienciaficcion.files.wordpress.com/2015/03/netflix-daredevil-individual-posters-matt-murdock.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/61ljrN7zmoL._AC_SL1024_.jpg",
      "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sabrina-temporada-2-netflix-poster-1552552891.jpg",
      "https://i.pinimg.com/originals/08/b9/a6/08b9a6aa4590cc8c08220684a292cb15.jpg",
      "https://posterspy.com/wp-content/uploads/2019/04/lucifer-Tom-Ellis-netflix-poster.jpg"
    ];
    List<String> _titulos = [
      "assets/images/daredevil.png",
      "assets/images/13.png",
      "assets/images/sabrina.png",
      "assets/images/resident.png",
      "assets/images/lucifer.png"
    ];
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _imagenes.length,
        itemBuilder: (context, index) {
          return Stack(alignment: AlignmentDirectional.bottomCenter, children: [
            Container(
              margin: new EdgeInsets.symmetric(horizontal: 5.0),
              height: 110.0,
              width: 110.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(110.0),
                  border: Border.all(color: Colors.yellow, width: 2.0)),
              child: ClipOval(
                  child: Image.network(_imagenes[index], fit: BoxFit.cover)),
            ),
            Image.asset(
              _titulos[index],
              width: 100.0,
            )
          ]);
        });
  }
}
