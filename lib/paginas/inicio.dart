import 'package:clone_netflix/componentes/cartel_principal.dart';
import 'package:clone_netflix/componentes/item_image.dart';
import 'package:clone_netflix/componentes/item_redondeado.dart';
import 'package:flutter/material.dart';

class InicioPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: ListView(
        children: <Widget>[
          CartelPrincipal(),
          this.listaHorizontal('Avances', ItemRedondo(), 10),
          SizedBox(
            height: 10.0,
          ),
          this.listaHorizontal(
              'Programa sobre viajes en el tiempo', ItemImage(), 10),
          SizedBox(
            height: 10.0,
          ),
          this.listaHorizontal('Tendencias', ItemImage(), 10),
          SizedBox(
            height: 10.0,
          ),
          this.listaHorizontal('Mi lista', ItemImage(), 10),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
      bottomNavigationBar: this.navInferior(),
    );
  }

  BottomNavigationBar navInferior() {
    return BottomNavigationBar(
      backgroundColor: Colors.black,
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white24,
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Inicio'),
        BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Buscar'),
        BottomNavigationBarItem(
            icon: Icon(Icons.library_music), label: 'Proximamente'),
        BottomNavigationBarItem(
            icon: Icon(Icons.arrow_downward), label: 'Descargas'),
        BottomNavigationBarItem(icon: Icon(Icons.more_horiz), label: 'Mas'),
      ],
    );
  }

  Widget listaHorizontal(String titulo, Widget item, int cantidad) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 10.0),
            child: Text(titulo,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0)),
          ),
          Container(
            height: 110.0,
            child: item,
          ),
        ]);
  }
}
